# First Day Stuff

* No textbook, but readings.
  * Need to take seriously.
  * Need to bring readings to class with either a large-screened device or printed.
    * No Phone!!!
  * Readings are roughly 10 pages per assignment
* Absence Policy:
  * 4 fully excused absences
    * Each subsequent absences reduce letter grade by 1/3.
    * After 10 absences, you fail the class.
    * Exceptions will be made for University sanctioned events (SC).
  * Every 2 late arrivals or early departures count as 1 absence.
* Additional Rules:
  * Attendance (see above)
  * Revisions:
    * Full revisions are allowed for 2 papers.
    * Revision grade replaces the original grade.
  * Attention:
    * Pay attention, don't be distracted, don't distract others.
  * Civility and Openness:
    * Be civil and respect others.
    * Discussions, contractions, etc. are fine, but be respectful about it.
    * Be open to readings.

* Assignments:
  1. Informative Essay:
    * Way to demonstrate a more complex way of writing an essay using professional writing techniques to explain complex ideas.
    * Will show a very structured way of writing this type of report/essay.
    * Most "collegey" assignment we'll do.
  2. Homework
    * Write an email using the "How to Write an Email" link as a template.
    * Answer the following:
      * How do you do research?
        * Do you use something like PowerNotes?
      * Is it problematic to bring a laptop to class?
    * Send by the end of Monday.
