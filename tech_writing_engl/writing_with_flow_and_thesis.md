# Writing a Thesis Paragraph

* Hardest paragraph to write in the entire paper.
* The essay layout we use can be used to determine what exactly should go in the thesis paragraph.
  * Parts of the thesis:
    * Hook:
      * Should be narrowed on the question you're discussing in the paper.
      * Single sentence
      * _The hook can be gotten from the difference between the original argument and your argument._
        * Original Argument: Paragraph 1 (first after the thesis)
        * Your Argument: Paragraph 4
        * Example: the Warren-straw climate change arguments -> something like "what is the best solution for climate change?"
    * Intro (3-5 sentences):
      * Introduce the other side of the argument
      * Provide enough info to cleanly transition into the thesis statement
    * Thesis Statement:
      * One or two sentences explicitly stating your argument.
      * _The thesis can usually be gotten from paragraph 7._

# Writing with Flow

* IMO: Writing with flow is when one's arguments, ideas, opinions, etc. are well connected and tie into each other well. Usually, this is produced through the use of grammar tools like prepositional phrases which are used to draw the reader's attention back to previous points enough that they can be expanded upon by upcoming points.
* Requires the writer to anticipate the readers' thoughts.
* Dr. Wallace's Way to Write Well with Flow
  * Think of every sentence as posing a subconcious question.
    * The next sentence(s) answer that question, but might raise their own questions.
    * When all the questions are answered, end the paragraph.
    * Quotes also factor in here because, in academic writing, the reader will want to see some example of what you're saying (which is fulfilled by the quote).
