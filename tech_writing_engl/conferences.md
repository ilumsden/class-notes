# Conferences

* Bring a **paper** copy of the paper.
* Come with at least 1 question in mind.
* Should be a full draft
* Good formatting
* Email ASAP if _in crisis_.
