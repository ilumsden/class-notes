# Quoting and Sources

## Tips for Quoting:
* Things to provide leading into the quote:
  * Who the person is
  * Where the person published the quote
  * What the person is talking about
* Quoting and Analysis:
  * For longer quotes, a rule of thumb is about 1 line of analysis per line of quote.
  * When analyzing the quote, add your own opinions and ideas about it to further support your thesis.
    * This helps the reader focus into what you (as the writer) wants the reader to get from the quote.

## Tips for Sources:

* Questions to Consider:
  * Where is the evidence (for the source) from?
    * Examples:
      * Peer Reviewed Journals: YES
      * General Website: NO
  * Is the source fact-checked?
    * Examples:
      * Wall Street Journal Main Reporting: YES
      * New York Times Op-Ed Section: NO
  * Is it aiming at neutrality?
    * Not always important for a source (depends on what you're using the source for)
    * Examples:
      * Wikipedia: YES
      * New York Times Op-Ed: NO
  * Is the "bias", "angle", subjectivity, etc. understandable and managable?
    * If the source is massively subjective and controversial, using it as a source in your work can be detrimental to your argument.
    * Examples:
      * Vox.com: Centrist-Left political articles, can appeal to both left and right wing people
      * Jacobin: Far-left, almost Marxist political opinions, very subjective.
