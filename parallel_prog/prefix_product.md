# Prefix Product

## Example Problem:

__Given:__ M<sub>0</sub>, M<sub>1</sub>, M<sub>2</sub>, ... M<sub>n-1</sub> matrices, where each is _m x m_.

__Goal:__ Solve for the following:

> S<sub>0</sub> = M<sub>0</sub>
> 
> S<sub>1</sub> = M<sub>1</sub>M<sub>0</sub>
> 
> S<sub>i</sub> = M<sub>0</sub>M<sub>1</sub>M<sub>2</sub>...M<sub>n-1</sub>

_Called the Prefix Product_

## Serial Solution:

> S<sub>i</sub> = S<sub>i-1</sub>M<sub>i</sub>

Each operation is performed according to standard mathematical matrix multiplication.

__Complexity:__
* Single Square Matrix multiplication (_m x m_) = O(m<sup>3</sup>)
* Total Complexity: O(n)O(m<sup>3</sup>)

_Note: Matrix multiplication (for square matrices) is binary associative)_

## Parallel Solution:

### Steps:
1) Partition the set of matrices into _p_ partitions such that the product for each partition is __R<sub>i</sub> = M<sub>(in)/p</sub>...M<sub>(((i+1)n)/p)-1)</sub>__.
2) Create Top and Bottom matrices for communication (per partition).
    * Top Matrix: Running total for the product (per process).
    * Bottom Matrix: Depends on stage.
      * At beginning of round, matrix is received from other process.
      * By end, matrix equals top matrix.
3) Send/Recv 1
    * Group processes into pairs.
    * Swap bottom matrices between processes.
    * If sending process rank `<` current process rank, repeat 2.
    * Else, do nothing.
4) Send/Recv 2
    * Group the groups.
    * Pairwise swap of matrices between groups of processes.
      * Example, (1, 2), (3, 4) = 1 -> 3, 3 -> 1, 2 -> 4, 4 -> 2
    Update step is same as 3.
5) Repeat step 4 until reach answer.

### Complexity:

* Matrix Multiplication (Serial Part): O((n/p)m<sup>3</sup>)
* Parallel Part:
  * In each round of communication, we do at most 1 or 2 mat. muls per process (depending on rank condition): O(m<sup>3</sup>)
  * In `log(P)` rounds of computation, the cost is O(m<sup>3</sup>log(P)).
* Communication:
  * In `log(P)` comms, total communication cost is O(log(P))

__Total Parallel Cost:__ T(n, m, P) = O((n/p)m<sup>3</sup>) + O(m<sup>3</sup>logP) + O(logP) = __m<sup>3</sup>O((n/p)+logP) + O(logP)__
  * n = Number of matrices
  * m = Size of matrix (remember they're squares)
  * P = Number of partitions

## Speedup

As usual, speedup __S(P) = T<sub>serial</sub>/T<sub>parallel</sub>__ ~

```math
S(P, m, n, c) = \frac{nm^3}{m^3\frac{n}{P} + m^3log(P) + clog(P)}
```

## General Equation for Prefix Operations

For __S<sub>i</sub> = S<sub>i-1</sub> ? a<sub>i</sub>__, where `?` is the operation:
```math
T(n, P, c) = O(c\frac{n}{P} + clog(P) + c_{comm}log(P))
```

where c is a factor of the operation (i.e. m<sup>3</sup> for matrix multiplication) and c<sub>comm</sub> is the cost of communication.

## Sample Problem:

Solve for x<sub>2</sub> using the following:
* x<sub>i</sub> = a*x<sub>i-1</sub> + b*x<sub>i-2</sub>
* a and b are known constants
* x<sub>0</sub> and x<sub>1</sub> are given.

### Solution:

1) Rephrase the problem equation (first bullet) as:
```math
\begin{bmatrix}
x_i & x_{i-1}
\end{bmatrix} = \begin{bmatrix}
x_{i-1} & x_{i-2}
\end{bmatrix} * \begin{bmatrix}
a & 1 \\
b & 0
\end{bmatrix}^{i-1}
```
2) Solve the matrix power problem using Prefix Product.
3) Do simple sequential matrix-vector multiplication to get final answer.

__*Note: Prefix operations are so common that MPI has `MPI_Scan`, which takes a function representing a binary associative operation and uses it to compute the operation.*__

## Speedup for Hybrid Runtimes

If your code has a parallelized part and a non-parallelizable part, the runtime of the partially-parallelized code is as follows:

```math
T_p = \epsilon*T_s + \alpha*\frac{(1-\epsilon)T_s}{P}
```

where epsilon is the sequential fraction of the run (0 to 1), alpha represents the efficiency of the parallelizable part (>= 1, 1 -> embarrisingly paralllelizable), and T<sub>s</sub> is the speed of the program if it were fully sequential.

Thus, the speedup is

```math
S(P) = \frac{1}{\epsilon + \alpha\frac{1-\epsilon}{P}}
```

### Efficiency

Iso-Efficiency (generally just called efficiency) is

```math
E(P) = \frac{S(P)}{P}
```
