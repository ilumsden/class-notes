# Models and the Viewing Pipeline

## Polygon Mesh:

* Two things are required:
  - Topology
    - Usually represented as a table of vertex coordinates (basically an array of structs).
  - Connectivity
    - Usually represented as the following:
      - A table of edges connecting two vertices (usually refers to indices from the vertex coordinate table).
      - Sometimes, a table of polygonal surfaces (usually refers to indices from the edge table).
* In the end, __Triangle Meshes__ beat out other types.
  - Programmatic Representation:
```c
// tmesh.h

#define VERTICES_PER_OBJ 3;
#define NUM_DIMENSIONS_OBJ 3;

typedef struct mesh_t
{
    /* Stored as sequences of (x, y, z) */
    float *vertex_array;
    /* Stored as index sequences of (v1, v2, v3) */
    int *triangle_array;
    int num_vertices;
    int num_triangles;
} *tmesh;

// tmesh.c

tmesh new_mesh(/* input */)
{
    /* Parse input */
    tmesh m;
    m - malloc(sizeof(mesh_t));
    m->num_vertices = /* Val from input */;
    m->num_triangles = /* Val from input */;
    m->triangle_array = malloc(/* Num from input */ * sizeof(int) * VERTICES_PER_OBJ);
    m->vertex_array = malloc(/* Num from input */ * sizeof(float) * NUM_DIMENSIONS_OBJ);
    /* Enter initial values depending on input */
    return m;
}
```
