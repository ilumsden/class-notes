# First Day Stuff

## Professor: Jian Huang
* Office: MK 323
* Office Hours: _By Appointment_
* Email: huangj@utk.edu

## TA
* John Duggan
* Email: jduggan1@utk.edu
* Office: MK 314
* Office Hours: Wed. 2:30-3:30 or by appointment

## Hydra Class Directory: ~huangj/cs456

## Website:
* web.eecs.utk.edu/~huangj/cs456/
* All info for the course is on the website, including:
  * Lecture Notes
    * Slides this semester
  * Lab Write-Ups
  * Most of this first day info.
  * etc.

## Important Dates:
* Midterm: Thurs., March 26
* *__No Final__*: Final project will be done in lieu of a final
* Last Class: April 25
* Spring Break: March 18-22

## Labs:
* No lab session. Labs are to be completed on your own time.
* All labs are due by midnight on the due date.
  * Due date is planned to be on Thursdays.
* Labs will be submitted to a repo. It will either be SVN or GitHub.
* This semester there will be early submission extra credit.
* The first lab will be in C/C++. The rest (and the final project) will be in JavaScript.
* Each student will get one late-submission waiver.
* Labs should be written so that it can be reusable. It will be needed for the final project.

## Grading:
* Lab 1: 10 pts
* Lab 2: 15 pts
* Lab 3: 20 pts
* Midterm: 30 pts
* Final Project: 25 pts
* Class Participation: 5 pts extra credit

## Content:
* Graphics Basics:
  * Fundamentals of Display Hardware
  * Graphics Applications
  * Interactive Techniques and Color Models
  * 3D Viewing pipeline
  * 3D Polygon Rendering (clipping, scan conversion, and visibility algorithms)
  * Illumination Models and Global Illumination
  * Shading Models and Shading Language
  * Ray Tracing (YAY!!!)
  * Realistic 3D Rendering (for imagery)
    * Texture Mapping
    * Anti-Aliasing
    * Radiosity
    * Bidirectional Reflectance Distribution Function (BRDF)
  * Brief Overview of:
    * Scene Modeling
    * Computer Animation
    * Volume Visualization
    * Image-based Rendering
* Graphics Programming
  * All OpenGL
    * OpenGL Fixed Functionality
    * OpenGL State Machine
    * OpenGL Programmable Pipeline
    * Shader API
    * Scriptable Graphics Engines
    * Etc.
