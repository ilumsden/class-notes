# Intro to Computer Graphics

## The Rendering Equation:
*Best to look at this in the slides.*

`I(x, x') = g(x, x')[\epsilon(x, x') + \integral_S(\rho(x, x', x'')*I(x', x''))dx'']`
* `I(x, x')` = intensity passing from x' to x
* `g(x, x')` = geometry term (1, or `1/(r^2)`, if visible from x'; 0 otherwise)
* `\epsilon(x, x')`: intensity emitted from x' in direction of x
* `\rho(x, x', x'')` = scattering term for x' (fraction of intensity arriving at x' from the direction of x'' scattered in the direction of x)
* `S` = union of all surfaces

This equation is almost impossible to solve (computers use iterative algorithms), and, in fact, this equation will never converge in certain scenes. As is standard with computer algorithms, this equation will almost never be solved analytically.

However, for computer graphics, this equation is massively simplified because the viewer is always at a single point. Thus, `x` is constrained to a single value. Also, for `\rho`, most materials have very predictable, uniform scattering patterns. This massively simplifies the evaluation/approximation of `\rho`. Also, in many cases, most of the global illumination (`I(x, x')`) can be ignored. Due to all these simplifications, this equation is __almost never__ directly used. Essentially, the more photo-realistic the scene is, the closer the approximations get to trying to solve this equation.

## Modeling Color: Physics
* It's all based on ElectroMagnetic Radiation.
  - Different colors correspond to radiation of different wavelengths.
  - Intensity of each wavelength is specified by amplitude
    - Frquency = 2 * pi / wavelength (*Is this actually right?*)
      - This equation will be discussed more during the lectures on __Aliasing__.
* Humans can only see EM waves between 400-700 nm wavelengths.
  - As a result, graphics only have to consider a small range of values for color modeling.
* Color and Wavelength:
  - Most light we see is not just a single wavelength, but a combination of wavelengths.
  - This is usually referred to as a spectrun (or spectral color distribution).
* 3-Component (Tristimulus) Color:
  - The de-facto representation of color on displays is RGB (*additive color*).
  - Some printers use CMYK (*subtractive color*).
  - Why?:
    - Color is human sensation:
      - Cone (Color) and Rod (Intensity) receptors in the retina.
      - There are 3 types of Cone receptors with peak sensitivities of 430 nm (roughly R), 560 nm (roughly G), and 610 nm (roughly B) for the "average" observer.
    - Standard Color:
      - There is none.
      - It is based on the biology of the individual. Every individual sees color slightly differently.
      - The best approximations are developed through human color matching experiments.
      - This lead to multiple color spaces for graphics.
  - Main Color Spaces:
    - CIE XYZ, xyY
    - RGB (monitor), CMYK (color printing) (both for display)
    - HSV (Munsell, HSL, IHS) (for color/itensity editing)
    - Lab, UVW, YUV, YCrCb, Luv (for compression)

## Modeling Eye:

* Pin Hole Camera:
  - Very simple model to simplify math.
  - Basic Design:
    - Similar to an old camera.
    - Imagine a perfect cube with an extremely small, perfectly round hole in a face.
    - The hole is the "eye".
* The Viewer
  - Viewing involves camera position and direction.
  - Like a human, the viewer perceives 3D objects as 2D.
    - For example, consider two parallel rows of equidistant trees. When you look at it, it actually looks as though the two rows of trees are converging towards the same point.
* Modeling the Retina: Frame Buffer, Image, Rasterization
  - Tech made cheap by TV boom.
  - Image is a 2D display that stores pixel values.
  - The frame buffer is used to allow for rapid refresh of an image on the display.
    - Entire screen is painted some number of times (say 30) per second.
    - Entire screen is traversed some other number of times (say 60) per second.
    - Refresh occurs by altering the pixel setup one-by-one. The refresh travels horizontally until the end of the current row. It then goes to the next row.
  - Image rendering needs to occur at 30 FPS or more to look continuous.
    - This speed (and resolution) is limited by how fast the display can perform a full refresh of the image.
* Modeling Geometry:
  - Usually done in 3 frames of reference (x, y, and z).
  - Software merges them together into a "3D" model.
  - Geometry modeling is usually done by artists (or engineers in things like CAD). It is not part of the realm of Computer Graphics.
  - __Patch__:
    - The representation of curves in models.
    - Based on Bezier Patches (and curves).
      - The user manipulates the Bezier Patch. This causes deformation of the overall, more complex patch based on Bezier's algorithms.
  - The patch is used primarily for modeling. It is not used for rendering.
    - Patches are usually converted to Polygon (really __Triangle__) __meshes__.
      - Triangle Meshes won because *all* polygons and 3D objects can be reduced to combinations of triangles, and triangles are always guaranteed to be 2D.
* Modeling Materials:
  - Usually achieved by a combo of shading and texturing.
* Modeling a Scene:
  - Combine everything together.
