# Introduction to Operating Systems

## Definitions
* __Operating System__: software that controlls hardware and links software to hardware.
* __Kernel__: the executable containing the OS code.
* __Input__: data coming from hardware into an OS
* __Output__: data coming from the OS to hardware/user

## Abstraction
* OSes provide an abstraction and interface for applications (other software) to use.
  * Makes it so that you don't have to code towards particular hardware.
  * OSes deals with the smaller details/hardware support. They allow users to write generic code for any (or nearly any) hardware.

## Virtualization
* The OS can use virtualization to make programs in the user space believe it has things that it doesn't.
  * Examples:
    * OSes can make uniprocessor systems appear to be multiprocessor systems (context switching).
    * OS can schedule which processor gets which task.
    * OS can make all file systems look the same.

## Services:
* OSes usually offer "services", such as priorities on tasks, network stack, etc.
* OSes usually acts as a resource manager, especially in multi-user environments.
* Most OS Services are represented as *system calls*.
