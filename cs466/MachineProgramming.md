# Machine Programming

## Basics:

### Intel x86 Processors:
* Dominate the laptop/desktop/server market
* Evolutionary design processors:
  * All Intel x86 processors are essentially the same chip with more features added on year after year.
  * Backwards compatible up to the Intel 8086.
* CISC (Complex Instruction Set) Processor series
* AMD also uses Intel's x86 instruction set.
* Today, processors are based on x86-64, a 64-bit variant of x86.
  * x86-64 was actually introduced by AMD (now called "AMD64").
* x86 architecture led to the rise of multicore processors.

### Definitions:
* Architecture:
  * Also referred to as ISA (Instruction Set Architecture)
  * The parts of processor design that someone needs to know to understand or write assembly/machine code.
* Microarchetecture:
  * Implementation of the architecture
* Code Forms:
  * Machine code: binary instructions that the processor directly runs.
  * Assembly Code: A textual representation of the machine code. Instructions are based on ISA.

### Assembly/Machine Code View:
* Things that the programmer can see/affect
  * Program Counter (PC):
    * Contains address of next instruction
    * Called *RIP* in x86-64
  * Register File
    * Heavily used program data
  * Condition Codes
    * Store status info about most recent arithmetic or logical ops.
    * Used for Branching.
  * Memory:
    * Byte addressable array
    * Code and user data
    * Stack and Heap

### Turning C into Object Code:
1. Compiler: Turns C text files into an ASM program
  * For gcc, the -Og flag disables most optimizations.
  * The -S flag tells gcc to only generate the ASM code.
2. Assembler: Turns ASM into binary object files.
  * ASM is assembly code.
3. Linker: Turns object files into executable binary.

### Assembly Characteristics:
* Data Types:
  * "Integer" data of 1, 2, 4, or 8 bytes
    * Both int values and pointers.
  * Floating point data of 4, 8, or 10 bytes
  * Code: byte sequences encoding instructions.
  * No aggregate types
* Operations:
  * Arithmetic function on register or memory data.
  * Transfer data between memory and register.
    * Load and Store operations
  * Transfer Control:
    * Unconditional jumps
    * Conditional branching

### Disassembler:
* _objdump_
  * A useful disassembler tool for examining object code.
  * Analyzes the bit pattern of series of instructions.
  * Produces an approximate rendition of assembly code.
  * Can be run on either executables or object files.
  * Part of the GNU stack, so it's usually downloaded by default on Linux.

### Basic Instructions:
* Moving data:
  * `movq src, dest;`
  * Moves data from `src` to `dest`
  * `src` and `dest` can be:
    * Immediate value:
      * Example: constant integer data like `$0x400` or `$-533`
      * Similar to C consts, but prefixed with "$"
    * Registers (such as $rax, $r13, etc.)
      * Note: $rsp (and some others) is reserved for special use
    * Memory: works the same as immediate values, but with a variable name and a % instead of values and a $.
  * Cannot do memory to memory transfers with a single instruction (i.e. *ptr1 = *ptr2)
    * Instead, the value at ptr2 has to be moved to a register. The value can then be moved from the register to the new memory address.
  * The move instruction can perform shifts too by placing the byte shift (right) after `movq`.
    * Example: `*ptr1 = ptr2 + 8` becomes `movq 8 (%rbp), %rdx`
* Complete Memory Addressing Modes:
  * General Form: Mem[Reg[Rb] + S*Reg[Ri] + D] -> D(Rb, Ri, S)
    * D: constant dispacement (1, 2, 4, etc. bytes)
    * Rb: Base Register (any of 16 integer registers)
    * Ri: Index Register (Any, except for %rsp)
    * S: Scale (1, 2, 4, or 8 only)
* leaq: computes address from a variable
  * `leaq address, dest`
  * `address` is an address expression (see Complete Memory Addressing Modes)
  * `dest` is a register that `address` is stored in.
* salq: bit shift
  * `salq value, dest`
  * `value` is the value of the shift (ex: $8, %mem, etc.)
  * `dest` is a register that the resulting post-shift value is stored in
* Many other types of instructions (especially arithmetic) can be found in the lecture notes.
  * Note that arithmetic operations do __not__ have distinct signed and unsigned forms
