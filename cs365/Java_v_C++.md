# Java Vs. C++

## Environment Issues:

1. The JVM allows Java code to be computer unspecific. Unlike C++ code, which will only work on a computer with the same hardware and software components, Java code will work on any other computer with a version of the JVM that is compatible with the code.

2. The JVM provides a lot of run-time checks to ensure the program does not breach security on the platform. These cause the language to be safer, but slower, than C++. Some examples are:
  * Array bounds checking
  * Type checking of downcasts
  * Garbage collection so that the user does not have to have to handle memory deallocation and dangling pointers.
  * Prevention of explicit manipulation of pointers.

## Memory Management:
* Java has __no__ memory reclamation operators (i.e. `delete` in C++ and `free` in C). Java uses a garbage collecter to perform memory reclamation.
* Java has __no__ explicit pointers. Pointers do exist in Java, but only internally.
  * Implicitly, __everything__ in Java that is __not a primitive__ is a pointer.
  * However, you treat these pointers like C++ objects.
* Java eliminates value (i.e. stack-allocated) objects. __All objects__ (non-primitives) are allocated off the heap.
