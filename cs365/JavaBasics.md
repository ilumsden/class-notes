# Java Basics

## Java and C++
* Java was originally made for set-top boxes as an improvement on C++.
* At the advent of the Internet, Java was repurposed for the Internet, causing its explosion of popularity.
* Because Java was meant to be an improvement on C++, much of the basic syntax is the same.
  * Examples:
    * Looping
    * Variable declaration (except that `bool` is `boolean`)
    * Many more

## Basics

### Main function
Java puts `main` in a class with strict syntax. The following is a basic Hello World program.
```
class HelloWorld {
    public HelloWorld() {
        System.out.println("Hello World");
    }

    static public void main(String args[]) {
        new HelloWorld();
    }
}
```
* `main` must have the signature `static public void main(String args[])`
* User-Provided Arguments:
  * Java does not provide an `argc` value because the array type has a `len` function that can provide it.
  * User input in the `args` array starts at index *__0__*. For comparison, in C and C++, user input in `argv` starts at index 1, and index 0 represents the name of the program.

## Compilation and Running

* Process:
  * Run `javac` on the `.java` file that you want to compile.
  * `javac` compiles the Java code into a `.class` file, which contains _bytecodes_.
    * _bytecodes_ are essentially assembly files for JVM.
    * Because bytecodes are for JVM, they are portable between machines with compatible versions of the JVM.
  * To run a bytecode file, use `java MainName`, where `MainName` is the name of the bytecode file (without the `.java` or `.class`) that contains your main function.
  * When run, the JVM uses one of the following compilation methods to speed up the code:
    * *__Just-in-Time__* (JIT): Standard, on-the-fly compilation with a single restricted level of optimization.
    * *__HotSpot__*: A variant of a JIT compiler that watches for frequently run code segments (called _Hotspots_) and further optimizes them.

## Writing Code

1. Don't put non-static functions in `main`:
  * `main` is a `static` member method. This means that it is essentially a global. Due to Java's (reasonable) restrictions, no non-static methods can be called from `main`.
  * The work around is to essentially use the `public` constructor of the class that `main` is in as the actual `main`. In `main` itself, all that will happen is `new Constructor()`.
    * Example:
```
class Arithmatic {
  public Arithmatic() {
    sum(3, 6);
    multiply(9, 5);
  }

  int sum(int x, int y) { return x + y; }
  int multiply(int x, int y) { return x * y }

  public static void main(String args[]) {
    new Arithmatic();
  }
}
```

## Built-In Types:
* boolean: true/false
* char: character
* byte: 8-bit integer
* short: 16-bit integer
* int: 32-bit integer
* long: 64-bit integer
* float: 32-bit floating point number
* double: 64-bit floating point number

## The String Class:
* Differences from C++:
  * Java strings are immutable. In C/C++, you could change each character in a string because a string is just an array, not a class. In Java, strings are an immutable class.
    * To access the value of a character in a Java String, use the `charAt` member function. It returns a *copy* of the character at the passed index.
  * Java Strings __should not__ be compared with comparison operators (`==`, `<`, `>`, `<=`, `>=`, etc.).
    * To compare for equality, use the `equals` member function.
      * You can use `==` with strings, but it will compare memory addresses.
    * For general comparisons, use the `compareTo` member function.
  * If you want to modify or edit a string, use the following classes. Note that they have the same interfaces, so they are generally interchangable.
    * Use `StringBuilder` for single-threaded applications because it does not contain internal locking and race condition prevention.
    * Use `StringBuffer` for multi-threaded applications becuase it protects against race conditions.

## Type Conversions and Type Comparisons:
* Java automatically performs type conversion if the conversion does __not cause a loss of precision__.
  * Java errors when it cannot do an automatic cast.
  * Examples:
    * Storing a float in a double is OK, but storing a double in a float raises an error.
    * Storing a short in an int is OK, but storing an int in a short raises an error.
  * You can override the automatic casting and error raising by manually casting with a *C-style cast* (i.e. (int) variable).
  * All these casting rules are also applied to user-defined types/classes.
    * If you do manual casting on user-defined types/classes, Java will compile, but it will put a Runtime check on the cast to see if it is allowed.

## Arrays:
* Array declaration and initialization:
  * Two ways to declare a string:
    * `type var [];`
    * `type[] var;`
  * Array initialization occurs dynamically **on the heap**. You __must__ use the new keyword to create a new array.
    * `int[] nums = new int[10];`
    * There is __one__ execption to this rule. Java supports initializer list construction.
      * `String weekdays[] = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };`
* Arrays are __objects__, not built-in collections.
  * As a result, Java Arrays store __pointers, not objects__.
    * For example, when you create a new array of strings, the array contains pointers to string objects, which are initialized to `null`.
    * You __must__ initialize the elements of an array to something meaningful.
* Multidimensional Arrays:
  * Like in C++, multidimensional arrays are actually arrays of arrays.
  * In Java, you __must__ provide sizes of all dimensions except the last one. The last dimension (i.e. the second in a 2D array) can have a specified size, but it is not necessary.
    * This allows you to have arrays of different sizes in the last dimension.
* Looping over Arrays:
  * Two-types
    * Classic C-style:
      * An index-based loop.
      * This is the go-to loop for changing elements in the array as you loop through it.
      * Example:
```
String[] names = new String[10];
for (int i = 0; i < names.length; i++)
{
    names[i] = "";
}
```
    * Python/C++11 Style:
      * Range-based for loop.
      * Iterates over each element in the array, creating a __copy__ of the current element.
        * Because of this, this looping is not good for editing an array while looping over it.
      * Example:
```
String[] names = new String[10];
for (String s : names)
{
    System.out.println(s);
}
```

## Enumerated Types (Enums)
* Available since Java 1.5
* Enums are actually a class, and, thus, each enumerated constant is an object of the class.
  * This allows enums to be declared independently of other classes. Thus, they can be put in its own `.java` file.
* Usually, when using enums, you must prefix an enum value with the enum type (i.e. for enum Day, use Day.FRIDAY).
  * There are exceptions, such as switch statements. When you switch on an enum variable, Java deduces the enum type from the variable, so you don't have to prefix the enum value.
* Enums can contains member variables and functions, just like any class.
  * Member variables should usualy be declared `private final`
  * In this case, the enum type should have a constructor. All enum types should be declared in the enum with the constructor being invoked.
    * Example:
```
enum Planet
{
    MERCURY(..., ...);
    VENUS(..., ...);
    EARTH(..., ...);
    .
    .
    .

    private final double mass;
    private final double radius;

    Planet(double m, double r) {
        this.mass = m;
        this.radius = r;
    }
    
    // Other member functions
    .
    .
    .
};
```

## Garbage Collection
* Java utilizes a garbage collector to handle memory reclamation.
  * General Process:
    * JVM looks over the program's heap and checks each variable's reference counter (see below).
      * If the reference counter is 0, JVM makes a note of it and continues.
      * Otherwise, JVM just moves on.
    * Every so often, the JVM garbage collector activates.
    * The garbage collector checks the list of memory with a reference counter of 0.
    * For each piece of memory, the garbage collector frees the memory for reuse.
* Reference Counter:
  * A counter that states how many variables reference the underlying piece of memory on the heap.
  * When the counter hits 0, the memory is unused, and the garbage collector frees it.
  * Disadvantanges:
    * A reference counting garbage collector (like in Java and Python) cannot free cyclical data structures because it will always think that some part of the data structure is being use.
      * An example of such a cyclical data structure is a doubly linked list (and a cyclical graph).
