# First Day Stuff:

## Professor: Dr. Vander Zanden
* Office Hours: T: 2-3, W: 3:15-4:15 in MK 312
* Website: web.eecs.utk.edu/~bvz/cs365/
  * Includes things like a schedule.
  * BVZ records his lectures and posts them in the schedule on the website.
* Email: bvanderz@utk.edu

## TAs
* Sagarvarma Sayyaparaju:
  * Email: ssayyapa@utk.edu
  * Office Location and Hours TBA
* Michael Price:
  * mprice35@utk.edu
  * Office Location and Hours TBA
* TA Office Hours are mostly for grade correction.
  * The TAs will automatically give a grade of 0 if code doesn't work on Hydra. In this case, go see the TAs and show them that it works on your machine. They should give you most of your points back.

## Piazza used for all communication (+ emails).
Sign-up on website.

## Exam Schedule:
* Midterm: Tues. Mar. 5
* Final: Wed. May 1 from 12:30-2:30 pm
  * Not Comprehensive. Only 2nd half of course.

## Quizes
* There are after-class quizes each lecture.
* Assigned at 2 pm on day of lecture.
* Due just before the next lecture.
* The three lowest quiz grades are dropped.
* There will be __no__ extensions on quizes.
* There will be 28 quizes during the semester.

## Topics:
* Java (First 5-6 weeks)
  * Inheritance
  * ? (Can't remember the second one)
  * Exception Handling
  * Generics
* Data Storage and Retrieval
* Python
* Concurrency (in Java) (Ugh)
* Functional Languages

## Assignments:
* Turn in on Canvas
* Occassional pair assignments
* Both Homework and Quizes
* Homework is technically due at midnight before the due date, but there is a grace period until 6 am on the due date.
* *__Extra Credit__*:
  * Homework only
  * 24-hours in advance: 5 pts extra credit
  * 48-hours in advance: 10 pts extra credit
  * Based on last submission of assignment
* Late Penalty for Homework:
  * 1st day late: 10 pts (total) off
  * 2nd day late: 20 pts (total) off
  * 3 or more days late: No credit

## Attendance:
For the most part, attendance is not mandatory. The following are exceptions:
* Java Inheritance
* Python to end of course

## Textbook:
The textbook for this class is a ZyBook for Java.

## Grade Breakdown:
* 5%: Attendance
* 5%: Pre-class Zybook Activities
* 10%: After-Class Quizes
* 30%: Homework Assignments
* 25%: Midterm
* 25%: Final

### Note on Code:
All code is expected to work on the Hydra computers. Because of IT, these computers run on JVM 1.8. We wil be using *__Java v8__*. Do not use Java v9 because it will not work on Hydra (because of JVM 1.8).
