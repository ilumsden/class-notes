# Introduction

## Background

* Compiler/Translator
  * "A translator is a program that reads a program written in a source language and translates it into an equivalent program written in a target language."
    * Ex: old C++ compilers translated C++ code to C, and then used C compilers to reach machine code.
  * Types of Compilers:
    * Source-to-Source (e.g. C++ to C)
    * Binary-to-Binary (a variant of Cross Compilation)
    * Source-to-Binary
    * Binary-to-Source
  * History and Milestones:
    * __Pre-1952__: Most programming done in Assembly
    * __1952__: Grace Hopper writes first compiler for the A-O programming language
      * Quite inefficient, so not widely used.
    * __1957-1958__: John Backus and team write the first Fortran compiler
      * Widely used because it was the first compiler to use Optimization.
        * Many of these optimizations are still used today.

## Future
* Compiler construction is considered one of the biggest success stories of Comp Sci
  * Teaches a lot about how to handle complex software projects.
* Challenges for the Future (Mostly in the backend):
  * Improving performance of generated codes (optimization)
  * Applications of compilers in security, safety, and trustworthiness.
    * Rust!!!!!!!!
  * Multi-core, heterogeneous computing
    * HPC!!!!

## Knowledge Required for Implementing a Successful Compiler
* Programming Languages
  * Scopes, Lifetimes, Stack Frames, etc.
* Computer Architecture
* Formal Languages (wrt NFAs and DFAs)
* Algorithms
* Graph Theory
* Software Engineering

## Language Processing System
* Source program -IN> pre-processor -OUT> Modified Source program
* Modified Source -IN> Compiler -OUT> Target Assembly
* Assembly -IN> Assembler -OUT> Relocatable Machine Code (i.e. libraries or `.o` files)
* Relocatable Machine Code (including other libraries/`.o`s) -IN> Loader/Link-Editor -OUT> Executable

## Concepts Introduced in Chapter 1

### Phases

### Compiler Construction Tools

### Front and Back Ends

### Analysis-Synthesis Model

### Assemblers

### Linkers and Loaders
